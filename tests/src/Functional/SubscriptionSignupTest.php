<?php

namespace Drupal\Tests\recurly\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests basic subscription signup workflow.
 *
 * @package Drupal\Tests\recurly\Functional
 * @group recurly
 */
class SubscriptionSignupTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = ['field_test', 'recurly', 'recurly_test_client'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    // Associate Recurly subscriptions with user entities. And enable the
    // "silver" mock plan.
    $this->config('recurly.settings')
      ->set('recurly_entity_type', 'user')
      ->set('recurly_subscription_plans', [
        'silver' => [
          'status' => 1,
          'weight' => 0,
        ],
      ])
      ->save();
  }

  /**
   * Create a user and see if they can reach the signup page.
   */
  public function testSubscriptionSignupUserEntity() {
    $account = $this->drupalCreateUser(['manage recurly subscription']);
    $this->drupalLogin($account);

    $this->drupalGet('user/' . $account->id() . '/subscription/signup');
    $this->assertText('Silver Plan');
    // Without enabling either recurlyjs or recurly_host_pages you can't
    // get any further than this.
    $this->assertText('Contact us to sign up');
  }

}
