<?php

namespace Drupal\recurly_test_client;

/**
 * Mock for Recurly_Client provided by recurly/recurly-client.
 *
 * This class can be used to replace Recurly_Client when you want to return
 * canned responses instead of making queries to the API.
 *
 * Response data for mocked calls is stored in fixtures/ as XML files. These
 * are mostly copied from the Recurly PHP library, with some adjustments to
 * values to allow them to work better in scenarios where you need to be able
 * to associated a specific account ID with a subscription.
 *
 * If you use this Mock client in a test, you will need to ensure the
 * 'key_value_expire' table exists. Usually by adding something like this to
 * your test's setUp method for Unit/Kernel tests that don't have a full Drupal
 * install.
 *
 * @code
 *
 * public function setUp() {
 *   parent::setUp();
 *   $this->installSchema('system', ['key_value_expire', 'sequences']);
 *   RecurlyMockClient::clearResponses();
 * }
 *
 * @endcode
 */
class RecurlyMockClient {

  /**
   * Return debugging responses.
   *
   * To make it easier to enable this module, and then us it in the browser, and
   * help with debugging tests, you can add responses to this method. These
   * follow the same format you would get from addResponse(). There are a few
   * examples.
   *
   * @return array
   *   And array of responses in the same format as addResponse().
   */
  protected static function debugResponses() {
    return [
      'GET' => [
        '/accounts/abcdef1234567890' => 'accounts/show-200.xml',
        '/accounts/abcdef1234567890/invoices?per_page=20' => 'invoices/index-200.xml',
        '/accounts/abcdef1234567890/subscriptions?per_page=200' => 'subscriptions/index-200-single.xml',
        '/accounts/abcdef1234567890/subscriptions?per_page=50&state=active' => 'subscriptions/index-200-single.xml',
        '/accounts/abcdef1234567890/subscriptions?state=past_due' => 'subscriptions/empty-200.xml',
        '/invoices/1000' => 'invoices/show-200.xml',
        '/invoices/1001' => 'invoices/show-200-past_due.xml',
        '/plans' => 'plans/index-200.xml',
        '/plans/corporis_excepturi8/add_ons/dolores_molestiae15' => 'addons/show-200.xml',
        '/plans/silver' => 'plans/show-200.xml',
        '/subscriptions/32558dd8a07eec471fbe6642d3a422f4' => 'subscriptions/show-200.xml',
      ],
      'HEAD' => [
        '/accounts/abcdef1234567890/invoices?per_page=20' => 'invoices/head-200.xml',
        '/accounts/abcdef1234567890/subscriptions?per_page=50&state=active' => 'subscriptions/head-200-single.xml',
      ],
    ];
  }

  /**
   * Load responses from temporary storage.
   *
   * Responses are an ssociative array keyed by HTTP response methods. Each
   * element is an array where the key is the API path and the value is the name
   * of the fixture to use in the response relative to the fixtures/ directory.
   *
   * e.g. :
   * ['GET' => ['path/1' => 'fixture1.xml', 'path/2?foo=bar' => 'foo/bar.xml']]
   *
   * @return mixed
   *   Array of stored responses, or NULL.
   */
  protected static function loadResponsesFromTempStore() {
    /** @var \Drupal\Core\TempStore\SharedTempStore $tempStore */
    $tempStore = \Drupal::service('tempstore.shared')
      ->get('recurly_test_client');
    $responses = $tempStore->get('responses');
    $debugResponses = self::debugResponses();
    return $responses ? array_merge_recursive($responses, $debugResponses) : $debugResponses;
  }

  /**
   * Add a new response fixture.
   *
   * @param string $method
   *   HTTP request method, e.g. 'GET' or 'POST'.
   * @param string $uri
   *   API endpoint that the response should be returned for.
   * @param string $fixture_filename
   *   Path to the fixture file that contains the response to return.
   *
   * @throws \Drupal\Core\TempStore\TempStoreException
   */
  public static function addResponse($method, $uri, $fixture_filename) {
    $responses = self::loadResponsesFromTempStore();
    if (!isset($responses[$method][$uri])) {
      $responses[$method][$uri] = $fixture_filename;
      /** @var \Drupal\Core\TempStore\SharedTempStore $tempStore */
      $tempStore = \Drupal::service('tempstore.shared')
        ->get('recurly_test_client');
      $tempStore->set('responses', $responses);
    }
  }

  /**
   * Clear stored responses.
   */
  public static function clearResponses() {
    /** @var \Drupal\Core\TempStore\SharedTempStore $tempStore */
    $tempStore = \Drupal::service('tempstore.shared')
      ->get('recurly_test_client');
    if ($tempStore && $tempStore->get('responses')) {
      $tempStore->delete('responses');
    }

    return TRUE;
  }

  /**
   * Mock for Recurly_Client::request() that returns canned responses.
   *
   * @return \Recurly_ClientResponse
   *   Recurly response generated from the content of the fixture file used ot
   *   fulfill the request.
   *
   * @throws \Exception
   */
  public function request($method, $uri, $data = NULL) {
    $uri = str_replace('https://api.recurly.com/v2', '', $uri);

    $responses = self::loadResponsesFromTempStore();

    if (isset($responses[$method][$uri])) {
      $fixture_filename = $responses[$method][$uri];
    }
    else {
      throw new \Recurly_NotFoundError("Don't know how to $method '$uri'");
    }

    return $this->responseFromFixture($fixture_filename);
  }

  /**
   * Create a Recurly_ClientResponse from a fixture.
   *
   * @param string $filename
   *   Name of file that contains the canned response to load.
   *
   * @return \Recurly_ClientResponse
   *   Recurly response object populated with a canned response.
   */
  protected function responseFromFixture($filename) {
    $headers = [];
    $body = NULL;

    $fixture = file(__DIR__ . '/../fixtures/' . $filename, FILE_IGNORE_NEW_LINES);

    $matches = NULL;
    preg_match('/HTTP\/1\.1 ([0-9]{3})/', $fixture[0], $matches);
    $statusCode = intval($matches[1]);

    $bodyLineNumber = 0;
    for ($i = 1; $i < count($fixture); $i++) {
      if (strlen($fixture[$i]) < 5) {
        $bodyLineNumber = $i + 1;
        break;
      }
      preg_match('/([^:]+): (.*)/', $fixture[$i], $matches);
      if (count($matches) > 2) {
        $headerKey = strtolower($matches[1]);
        $headers[$headerKey] = $matches[2];
      }
    }

    if ($bodyLineNumber < count($fixture)) {
      $body = implode("\n", array_slice($fixture, $bodyLineNumber));
    }

    return new \Recurly_ClientResponse($statusCode, $headers, $body);
  }

  /**
   * Mocks the recurly client's getPdf() method. Returns a known string.
   *
   * @return string
   *   Mock value.
   */
  public function getPdf($uri, $locale = NULL) {
    return 'Here is that PDF you asked for';
  }

}
